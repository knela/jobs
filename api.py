"""
API to questions
"""
from eve.auth import BasicAuth
from eve import Eve
from eve_healthcheck import EveHealthCheck


class MyBasicAuth(BasicAuth):
    """ Module to validate the auth user to access the API data"""

    def check_auth(self, username, password, allowed_roles, resource,
                   method):
        return username == 'user' and password == 'senhaforte'


application = Eve(auth=MyBasicAuth)

# configure healthcheck
EveHealthCheck(application, '/healthcheck')

if __name__ == '__main__':
    application.name = 'question'
    # run as debug
    application.run(debug=True, port=8000)
