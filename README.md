# Proposta

Apresentar uma aplicação [Vue](https://vuejs.org/) que consuma uma API local disponibilizada. A aplicação deve requerer autentificação e permitir que novos usuários façam o registro. A aplicação terá, ao menos, 3 telas:
- Uma tela de login/cadastro
- Uma tela de filtro do conteúdo armazenado no banco
- Uma tela para interagir com o conteúdo no banco


### Requisitos

#### Guia de interface

Há total liberdade quanto ao design a ser adotado para apresentar o conteúdo do banco, porém alguns pontos devem ser obedecidos:

- As cores da empresa devem ser utilizadas
```
	$primary   = #a6ce38
	$secondary = #016db9
```
- Para ter acesso ao conteúdo do banco, deve ser necessário fazer login.
	- O banco espera o registro de novos usuários, armazenando os campos `username` e `password`
	- O acesso à API requer autentificação: `user:senhaforte`

#### Pontos mínimos necessários

- Documentação (de preferência em inglês)
- Uso da [Integração Continua](https://about.gitlab.com/features/gitlab-ci-cd/) do Gitlab para a criação de um contêiner válido no 
*registry* do repositório.

#### Bônus

- Um tipo de teste automatizado para validar o CI.


### Termos de entrega

A entrega da proposta pode ser feita a partir de um *Merge Request* neste repositório ou por meio de um *patch* gerado a partir do *diff*.
Pacotes compactados sem relação com o repositório não serão aceitos.


### Fazendo de uso da API

A API oferecida neste repositório faz de uso do *framework* [Eve](http://python-eve.org/) e de um contêiner [docker](https://www.docker.com/) com um banco [mongo](https://docs.mongodb.com/).

Com o contêiner do banco rodando, a API pode ser levantada com:

	$ python api.py

A API estará disponível na porta `8000`.